// Test for adding a `quantity--standard` to relationships
//this should pass, it works

const builder = require(`../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let limeConventionSrc = JSON.parse( fs.readFileSync( `./../../output/collection/conventions/log--input--lime/object.json` ) );
let limeConvention = new builder.ConventionSchema( limeConventionSrc );

let limeLogUUID = randomUUID();
let limeQuantityUUID = randomUUID();
let limeUnitUUID = randomUUID();
let conventionUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let percentageUnitUUID = randomUUID();
let limeTypeUUID = randomUUID();
let areaQuantity2UUID = randomUUID();

let plantAssetExampleAttributes = limeConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = limeConvention.overlays.plant_asset.validExamples[0].id;

let logCategoryExampleAttributes = limeConvention.overlays.log_category.validExamples[0].attributes;
let logCategoryUUID = limeConvention.overlays.log_category.validExamples[0].id;

// reading the example
let limeExtraQuantityExample = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    lime_log: {
        id: limeLogUUID,
        attributes: {
            name: "example lime log",
            status: "done",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id: logCategoryUUID
                }
            ]},
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: areaQuantity2UUID
                },
                {
                    type: "quantity--material",
                    id: limeQuantityUUID
                }
            ] }
        },
    },
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
    area_quantity2: {
        id: areaQuantity2UUID,
        attributes: {
            label: "area2"
        }
    },
    lime_quantity: {
        id: limeQuantityUUID,
        attributes: {
            label: "lime",
            measure:"value"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: limeUnitUUID
                }
            ] },
            material_type: { data: [
                {
                    type:"taxonomy_term--material_type",
                    id: limeTypeUUID
                }
            ] }
        }
    },
    lime_unit:  {
        id: limeUnitUUID,
        attributes: {
            name: "lbs_acres"
        },   
    },
    lime_type:  {
        id: limeTypeUUID,
        attributes: {
            name: "calcitic_lime"
        },   
    },
    log_category:
    {
        id: logCategoryUUID,
        attributes: logCategoryExampleAttributes
    }
};

// testing the example
limeConvention.validExamples = [ limeExtraQuantityExample ];

let test = limeConvention.testExamples();
//console.log(limeConvention.validExamples)
//console.log(limeConvention.validExamples[0])
//console.log(limeConvention.validExamples[1])
//console.log(test);