# Last Configuration Steps (Below is your actual readme section)

  We've created a README based on the project information you've created below.
  First, we will some latest steps you need to take to finish configuring your repo below.
  
## (FUNDAMENTAL) Use a non unique domain, so that our links work

  In order for the links to have a predictable form, you need to use regular URLs and not the unique domains that are a default for gitlab. 
  
  * In the sidebar in gitlab, in your repo, go to "Deploy" -> "Pages" and _uncheck_  the **Use unique domain** option. 
  
  If you don't do this, the domain will have a hash which will make it hard to reach, and all the links in the auto generated documents will fail.

## Add Environment Variables to GitLab CI and the local `.env` file

  In order for the CI pipelines to work, you will need to add some extra variables.

  Secret variables haven't been added yet. All possible variables are documented in the generated .env file as in comments.
  Worth of mention are:

  * `NPM_AUTH_TOKEN`: Used to publish a package with validation functions into NPM each time you udpate your conventions.

## Add Project Icons

  You can make the wiki look more custon by adding some graphic elements.

  * `favicon.ico`: This is the small icon dispalyed on browser tabs that visit your website. We've provided the JSON-schema icon as an example.
  * `logo.png`_: This should be the logo for your organization or project. We've provided a JSON-schema icon as an example.
  * `docusaurus-social-card.jpg`: This is shown as a preview when the link is shared in an application that previews links.

## Read tutorials guiding you through all the functionality

* [Here](https://our-sci.gitlab.io/software/json_schema_distribution/wiki/blog/using-the-convention-builder) you can find our blog.



# farm-convention-test

## Description

this is a longer description of my comprehensive question set that helps farmers do things

## **Validation** - _Both Input and Output Collections can be validated in a single NPM package here_

* [NPM Package](https://www.npmjs.com/package/farm-convention-test)
* [CDN Package](https://cdn.jsdelivr.net/npm/farm-convention-test/dist/module/farmos_schemata_validator.js)/Browser Version