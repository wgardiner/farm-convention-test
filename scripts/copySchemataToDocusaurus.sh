for file in $( find output/collection/conventions/ | grep schema.json )
  do
      path=$( sed 's/output\/collection\/conventions/scripts\/docusaurus\/static\/schemas/' <<< "$file" | sed 's/schema.json//' ); 
      mkdir -p $path
      cp $file $path
done;
