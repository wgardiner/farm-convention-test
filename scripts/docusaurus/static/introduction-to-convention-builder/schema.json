{
 "title": "Octavio's Orchard Vegetable Planting",
 "type": "object",
 "$id": "www.gitlabrepo.com/version/farmos_conventions/0.1.0/conventions/asset--plant--octavio_orchard_planting",
 "$schema": "https://json-schema.org/draft/2020-12/schema",
 "properties": {
  "plant_asset": {
   "title": "Plant asset",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "asset--plant"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "name": {
       "type": "string",
       "pattern": "((lettuce)|(tomato)|(corn))-([a-z0-9]{4})-([0-9]{2})",
       "description": "This regular expression represents the planting name format in this farm: the involved species name, a 4 character alphanumeric code and the current decade, represented by two further numbers."
      },
      "status": {
       "const": "active",
       "description": "In this farm, plantings are only entered when already active."
      },
      "archived": {
       "type": "string",
       "title": "Timestamp",
       "format": "date-time",
       "description": "The time the asset was archived."
      },
      "data": {
       "type": "string",
       "title": "Data"
      },
      "notes": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Notes"
      },
      "flag": {
       "type": "array",
       "title": "Flags",
       "description": "Add flags to enable better sorting and filtering of records.",
       "items": {
        "type": "string",
        "title": "Text value"
       }
      },
      "id_tag": {
       "type": "array",
       "title": "ID tags",
       "description": "List any identification tags that this asset has. Use the fields below to describe the type, location, and ID of each.",
       "items": {
        "type": "object",
        "properties": {
         "id": {
          "type": "string",
          "title": "ID of the tag"
         },
         "type": {
          "type": "string",
          "title": "Type of the tag"
         },
         "location": {
          "type": "string",
          "title": "Location of the tag"
         }
        }
       }
      },
      "geometry": {
       "const": null,
       "description": "Geometry will be stored into the 'field' convention, therefore we omit it here."
      },
      "intrinsic_geometry": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Geometry"
        },
        "geo_type": {
         "type": "string",
         "title": "Geometry Type"
        },
        "lat": {
         "type": "number",
         "title": "Centroid Latitude"
        },
        "lon": {
         "type": "number",
         "title": "Centroid Longitude"
        },
        "left": {
         "type": "number",
         "title": "Left Bounding"
        },
        "top": {
         "type": "number",
         "title": "Top Bounding"
        },
        "right": {
         "type": "number",
         "title": "Right Bounding"
        },
        "bottom": {
         "type": "number",
         "title": "Bottom Bounding"
        },
        "geohash": {
         "type": "string",
         "title": "Geohash"
        },
        "latlon": {
         "type": "string",
         "title": "LatLong Pair"
        }
       },
       "title": "Intrinsic geometry",
       "description": "Add geometry data to this asset to describe its intrinsic location. This will only be used if the asset is fixed."
      },
      "is_location": {
       "type": "boolean",
       "title": "Is location",
       "description": "If this asset is a location, then other assets can be moved to it."
      },
      "is_fixed": {
       "type": "boolean",
       "title": "Is fixed",
       "description": "If this asset is fixed, then it can have an intrinsic geometry. If the asset will move around, then it is not fixed and geometry will be determined by movement logs."
      },
      "surveystack_id": {
       "type": "string",
       "title": "Surveystack ID",
       "maxLength": 255
      },
      "quick": {
       "type": "array",
       "title": "Quick form",
       "description": "References the quick form that was used to create this record.",
       "items": {
        "type": "string",
        "title": "Text value",
        "maxLength": 255
       }
      }
     },
     "required": [
      "name",
      "status"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "termination": {
       "type": "object",
       "properties": {
        "data": {
         "title": "Terminated",
         "type": "object",
         "required": [
          "id",
          "type"
         ],
         "properties": {
          "id": {
           "type": "string",
           "title": "Resource ID",
           "format": "uuid",
           "maxLength": 128
          },
          "type": {
           "type": "string"
          }
         }
        }
       }
      },
      "file": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Files",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "image": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Images",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "location": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Current location",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "owner": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Owners",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "parent": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Parents",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "plant_type": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "minContains": 2,
         "uniqueItems": true,
         "contains": {
          "oneOf": [
           {
            "type": "object",
            "properties": {
             "type": {
              "const": "taxonomy_term--plant_type"
             },
             "id": {
              "const": {
               "$data": "/species_taxonomy/id"
              }
             }
            }
           },
           {
            "type": "object",
            "properties": {
             "type": {
              "const": "taxonomy_term--plant_type"
             },
             "id": {
              "const": {
               "$data": "/variety_taxonomy/id"
              }
             }
            }
           }
          ]
         }
        }
       }
      },
      "season": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "minContains": 1,
         "uniqueItems": true,
         "contains": {
          "oneOf": [
           {
            "type": "object",
            "properties": {
             "type": {
              "const": "taxonomy_term--season"
             },
             "id": {
              "const": {
               "$data": "/season_taxonomy/id"
              }
             }
            }
           }
          ]
         }
        }
       }
      }
     },
     "type": "object",
     "required": [
      "plant_type",
      "season"
     ],
     "additionalProperties": false
    }
   },
   "required": [
    "id",
    "attributes",
    "relationships"
   ],
   "description": "This overlay represents plants as they are encoded in Octavio's farm, a medium sized comercial orchard in Argentina."
  },
  "species_taxonomy": {
   "title": "Plant type taxonomy term",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "taxonomy_term--plant_type"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "status": {
       "type": "boolean",
       "title": "Published",
       "default": true
      },
      "name": {
       "enum": [
        "tomato",
        "lettuce",
        "corn"
       ],
       "description": "Limited to a subset of know crops."
      },
      "description": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Description"
      },
      "weight": {
       "type": "integer",
       "title": "Weight",
       "description": "The weight of this term in relation to other terms.",
       "default": 0
      },
      "maturity_days": {
       "type": "integer",
       "title": "Days to maturity"
      },
      "transplant_days": {
       "type": "integer",
       "title": "Days to transplant"
      }
     },
     "required": [
      "name"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "vid": {
       "type": "object",
       "properties": {
        "data": {
         "title": "Vocabulary",
         "type": "object",
         "required": [
          "id",
          "type"
         ],
         "properties": {
          "id": {
           "type": "string",
           "title": "Resource ID",
           "format": "uuid",
           "maxLength": 128
          },
          "type": {
           "type": "string"
          }
         }
        }
       }
      },
      "parent": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Term Parents",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "companions": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Companions",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "crop_family": {
       "type": "object",
       "properties": {
        "data": {
         "title": "Crop family",
         "type": "object",
         "required": [
          "id",
          "type"
         ],
         "properties": {
          "id": {
           "type": "string",
           "title": "Resource ID",
           "format": "uuid",
           "maxLength": 128
          },
          "type": {
           "type": "string"
          }
         }
        }
       }
      },
      "image": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Photos",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "required": [
    "id",
    "attributes"
   ],
   "description": "The species of the vegetable planted. This establishment only works with tomato, lettuce, and corn."
  },
  "variety_taxonomy": {
   "title": "Plant type taxonomy term",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "taxonomy_term--plant_type"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "status": {
       "type": "boolean",
       "title": "Published",
       "default": true
      },
      "name": {
       "type": "string",
       "pattern": "^((tomato)|(lettuce)|(corn))-",
       "description": "Our farm's format is enforced: the species goes first, than a dash, than the variety."
      },
      "description": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Description"
      },
      "weight": {
       "type": "integer",
       "title": "Weight",
       "description": "The weight of this term in relation to other terms.",
       "default": 0
      },
      "maturity_days": {
       "type": "integer",
       "title": "Days to maturity"
      },
      "transplant_days": {
       "type": "integer",
       "title": "Days to transplant"
      }
     },
     "required": [
      "name"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "vid": {
       "type": "object",
       "properties": {
        "data": {
         "title": "Vocabulary",
         "type": "object",
         "required": [
          "id",
          "type"
         ],
         "properties": {
          "id": {
           "type": "string",
           "title": "Resource ID",
           "format": "uuid",
           "maxLength": 128
          },
          "type": {
           "type": "string"
          }
         }
        }
       }
      },
      "parent": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Term Parents",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "companions": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Companions",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "crop_family": {
       "type": "object",
       "properties": {
        "data": {
         "title": "Crop family",
         "type": "object",
         "required": [
          "id",
          "type"
         ],
         "properties": {
          "id": {
           "type": "string",
           "title": "Resource ID",
           "format": "uuid",
           "maxLength": 128
          },
          "type": {
           "type": "string"
          }
         }
        }
       }
      },
      "image": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Photos",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "required": [
    "id",
    "attributes"
   ],
   "description": "The variety. The name has a fixed format, prepended by the species in lowercase, separeted by a dash from the variety name."
  },
  "season_taxonomy": {
   "title": "Season taxonomy term",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "taxonomy_term--season"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "status": {
       "type": "boolean",
       "title": "Published",
       "default": true
      },
      "name": {
       "type": "string",
       "pattern": "((spring)|(summer)|(winter)|(autumn))-20[0-9]{2}",
       "description": "Seasons are always a temperate season, separated by a dash from it's beggining year."
      },
      "description": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Description"
      },
      "weight": {
       "type": "integer",
       "title": "Weight",
       "description": "The weight of this term in relation to other terms.",
       "default": 0
      }
     },
     "required": [
      "name"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "vid": {
       "type": "object",
       "properties": {
        "data": {
         "title": "Vocabulary",
         "type": "object",
         "required": [
          "id",
          "type"
         ],
         "properties": {
          "id": {
           "type": "string",
           "title": "Resource ID",
           "format": "uuid",
           "maxLength": 128
          },
          "type": {
           "type": "string"
          }
         }
        }
       }
      },
      "parent": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Term Parents",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "required": [
    "id",
    "attributes"
   ],
   "description": "A season taxonomy term should exist for each working period, and logs and assets should be associated to the related seasons. Seasons are always a temperate season, separated by a dash from it's beggining year."
  }
 },
 "description": "## Purpose\n\nThis is the main entity that represents a cultivar. It will be referenced by all subsequent managment logs.",
 "required": [
  "plant_asset",
  "species_taxonomy",
  "season_taxonomy"
 ]
}