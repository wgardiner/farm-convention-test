const esbuild = require('esbuild');

esbuild.build({
    entryPoints: [ './output/validators/allFarmOSSchemas.js' ],
    outfile: 'dist/browser/farmos_schemata_validator.js',
    globalName: 'farmosSchemas',
    bundle: true,
    format: 'iife',
    platform: 'browser',
    target: 'es6',
    minify: true,
    keepNames: true,
    sourcemap: true
}).catch(e => {
    console.error(e);
    process.exit(1);
});


esbuild.build({
    entryPoints: [ './output/validators/allFarmOSSchemas.js' ],
    outfile: 'dist/node/farmos_schemata_validator.js',
    bundle: true,
    format: 'cjs',
    target: 'es6',
    minify: true,
    keepNames: true,
    sourcemap: true
}).catch(e => {
    console.error(e);
    process.exit(1);
});


esbuild.build({
    entryPoints: [ './output/validators/allFarmOSSchemas.js' ],
    outfile: 'dist/module/farmos_schemata_validator.js',
    bundle: true,
    format: 'esm',
    target: 'es6',
    minify: true,
    keepNames: true,
    sourcemap: true
}).catch(e => {
    console.error(e);
    process.exit(1);
});


esbuild.build({
    entryPoints: [ './output/validators/allConventions.js' ],
    outfile: 'dist/browser/convention_schemata_validator.js',
    globalName: 'conventionSchemas',
    bundle: true,
    format: 'iife',
    platform: 'browser',
    target: 'es6',
    minify: true,
    keepNames: true,
    sourcemap: true
}).catch(e => {
    console.error(e);
    process.exit(1);
});


esbuild.build({
    entryPoints: [ './output/validators/allConventions.js' ],
    outfile: 'dist/node/convention_schemata_validator.js',
    bundle: true,
    format: 'cjs',
    target: 'es6',
    minify: true,
    keepNames: true,
    sourcemap: true
}).catch(e => {
    console.error(e);
    process.exit(1);
});


esbuild.build({
    entryPoints: [ './output/validators/allConventions.js' ],
    outfile: 'dist/module/convention_schemata_validator.js',
    bundle: true,
    format: 'esm',
    target: 'es6',
    minify: true,
    keepNames: true,
    sourcemap: true
}).catch(e => {
    console.error(e);
    process.exit(1);
});
